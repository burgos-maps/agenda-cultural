# agenda-cultural

geojson:

https://burgos-maps.gitlab.io/datos-abiertos-jcyl/eventos-culturales/9/ft3a-5csh.geojson


Eventos de la agenda cultural categorizados y geolocalizados

https://analisis.datosabiertos.jcyl.es/Cultura-y-ocio/Eventos-de-la-agenda-cultural-categorizados-y-geol/6ei4-jd8c

Acceso a este conjunto de datos a través de la API SODA:

https://analisis.datosabiertos.jcyl.es/resource/ft3a-5csh.json

https://dev.socrata.com/foundry/analisis.datosabiertos.jcyl.es/ft3a-5csh

Filtrado por código provincia = 9 (Burgos):


https://analisis.datosabiertos.jcyl.es/resource/ft3a-5csh.json?c_digoprovincia=9
